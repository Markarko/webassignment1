'use strict';

/**
 * @description function that changes the text color of an html element
 * @param {HTMLElement} selector 
 * @param {string} textColor 
 */
function changeTextColor(selector, textColor){
    if (selector !== undefined){
        selector.style.color = textColor;
    }
}

/**
 * @description function that changes the background color of an html element
 * @param {HTMLElement} selector 
 * @param {string} textColor 
 */
function changeBackground(selector, textColor){
    if (selector !== undefined){
        selector.style.backgroundColor = textColor;
    }
}

/**
 * @description function that sets the width of an html element in percentage
 * @param {HTMLElement} selector 
 * @param {number} width 
 */
function changeTagWidth(selector, width){
    if (selector !== undefined){
        selector.style.width = `${width}%`;
    } 
}

/**
 * @description function that changes the border color of an html element or collection of html elements
 * @param {[HTMLElement]} selectors 
 * @param {string} color 
 */
function changeBorderColor(selectors, color){
    if (selectors !== undefined){
        for (let i = 0; i < selectors.length; i++){
            selectors[i].style.borderColor = color;
        } 
    }
}

/**
 * @description function that changes the border width of an html element or collection of html elements in pixels
 * @param {[HTMLElement]} selectors 
 * @param {number} thickness 
 */
function changeBorderWidth(selectors, thickness){
    if (selectors !== undefined){
        for (let i = 0; i < selectors.length; i++){
            selectors[i].style.borderWidth = `${thickness}px`;
        } 
    }
}

/**
 * @description function that dynamically creates an HTMLElement and attaches it to a given parent
 * @param {string} elem 
 * @param {HTMLElement} parent 
 * @returns {HTMLElement} returns the created html element
 */
function createAndAttachElement(elem, parent){
    let newElem = document.createElement(elem);
    parent.append(newElem);
    return newElem;
}