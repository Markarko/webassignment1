'use strict';

window.addEventListener('DOMContentLoaded', () => {
    generateTable();
    addOnInputChangeEvents();
});
/**
 * @description function that add on change even listeners to all necessary inputs
 */
function addOnInputChangeEvents(){
    let rowCount = document.querySelector("#row-count");
    let colCount = document.querySelector("#col-count");
    let tableWidth = document.querySelector("#table-width");
    let textColor = document.querySelector("#text-color");
    let backgroundColor = document.querySelector("#background-color");
    let borderWidth = document.querySelector("#border-width");
    let borderColor = document.querySelector("#border-color");

    let elemsToAddEvents = [rowCount, colCount, tableWidth, textColor, backgroundColor, borderWidth, borderColor];

    for (let i = 0; i < elemsToAddEvents.length; i++){
        elemsToAddEvents[i].addEventListener("change", () => {
            generateTable();
        });
    }
}

/**
 * @description function that generates a table with all the values from the inputs
 */
function generateTable(){
    let tableRenderer = document.querySelector("#table-render-space");
    let tableHtml = document.querySelector("#table-html-space");

    let oldTable = document.querySelector("table");
    let oldHtml = document.querySelector("textarea");
    if (oldTable !== null){
        tableRenderer.removeChild(oldTable);
    }
    if (oldHtml !== null){
        tableHtml.removeChild(oldHtml);
    }

    let rowCount = document.querySelector("#row-count").value;
    let colCount = document.querySelector("#col-count").value;
    let tableWidth = document.querySelector("#table-width").value;
    let textColor = document.querySelector("#text-color").value;
    let backgroundColor = document.querySelector("#background-color").value;
    let borderWidth = document.querySelector("#border-width").value;
    let borderColor = document.querySelector("#border-color").value;

    let textarea = createAndAttachElement("textarea", tableHtml)

    let table = createAndAttachElement("table", tableRenderer);
    changeTagWidth(table, tableWidth);
    table.style.borderStyle = "dotted";
    changeBorderWidth(table, borderWidth);
    changeBorderColor(table, borderColor);
    textarea.value = "";
    textarea.value += "<table>\n";
    let tbody = createAndAttachElement("tbody", table)

    for (let i = 0; i < rowCount; i++){
        let tr = createAndAttachElement("tr", tbody)
        changeBackground(tr, backgroundColor);
        textarea.value += "  <tr>\n";
        for (let j = 0; j < colCount; j++){
           let td = createAndAttachElement("td", tr)
           td.textContent = "cell"+i+j;
           changeTextColor(td, textColor);
           changeBorderWidth([td], borderWidth);
           changeBorderColor([td], borderColor);
           td.style.borderStyle = "solid";
           textarea.value += "    <td></td>\n";
        }
        textarea.value += "  </tr>\n";
    }
    textarea.value += "</table>";
    textarea.rows = 1+rowCount*colCount+rowCount*2
}